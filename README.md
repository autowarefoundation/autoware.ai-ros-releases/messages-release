## autoware_msgs (melodic) - 1.13.0-1

The packages in the `autoware_msgs` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --track melodic --ros-distro melodic autoware_msgs --edit` on `Wed, 04 Dec 2019 02:13:17 -0000`

These packages were released:
- `autoware_can_msgs`
- `autoware_config_msgs`
- `autoware_external_msgs`
- `autoware_lanelet2_msgs`
- `autoware_map_msgs`
- `autoware_msgs`
- `autoware_system_msgs`
- `tablet_socket_msgs`
- `vector_map_msgs`

Version of package(s) in repository `autoware_msgs`:

- upstream repository: https://gitlab.com/autowarefoundation/autoware.ai/messages.git
- release repository: https://gitlab.com/autowarefoundation/autoware.ai-ros-releases/messages-release.git
- rosdistro version: `1.12.0-1`
- old version: `1.12.0-1`
- new version: `1.13.0-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.12`
- rosdep version: `0.15.2`
- rosdistro version: `0.7.4`
- vcstools version: `0.1.42`


## autoware_msgs (melodic) - 1.12.0-1

The packages in the `autoware_msgs` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release --track melodic --rosdistro melodic autoware_msgs --edit` on `Fri, 12 Jul 2019 21:22:59 -0000`

These packages were released:
- `autoware_can_msgs`
- `autoware_config_msgs`
- `autoware_external_msgs`
- `autoware_map_msgs`
- `autoware_msgs`
- `autoware_system_msgs`
- `tablet_socket_msgs`
- `vector_map_msgs`

Version of package(s) in repository `autoware_msgs`:

- upstream repository: https://gitlab.com/autowarefoundation/autoware.ai/messages.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.12.0-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.11`
- rosdep version: `0.15.2`
- rosdistro version: `0.7.4`
- vcstools version: `0.1.40`


## autoware_msgs (kinetic) - 1.12.0-1

The packages in the `autoware_msgs` repository were released into the `kinetic` distro by running `/usr/local/bin/bloom-release --track kinetic --rosdistro kinetic autoware_msgs` on `Fri, 12 Jul 2019 20:46:06 -0000`

These packages were released:
- `autoware_can_msgs`
- `autoware_config_msgs`
- `autoware_external_msgs`
- `autoware_map_msgs`
- `autoware_msgs`
- `autoware_system_msgs`
- `tablet_socket_msgs`
- `vector_map_msgs`

Version of package(s) in repository `autoware_msgs`:

- upstream repository: https://gitlab.com/autowarefoundation/autoware.ai/messages.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.12.0-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.11`
- rosdep version: `0.15.2`
- rosdistro version: `0.7.4`
- vcstools version: `0.1.40`


# messages-release

Holds ROS information for the messages repo necessary for release to the build farm.